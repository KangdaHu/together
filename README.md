#Together
***

###OverView
**Together**(一起吧) is defined as a platform for activity sharing. On it, users can create and lookup the interesting activities in which arbitrary users can participate.
***
###Features
* #####Sign up
At first, registration should process under the premise of invitation.

* #####Create Activity
Arbitrary user can create an activity and recruit other users taking part in it.

* #####Discussion
Users can discuss an activity on activity's page.

* #####Personalized user profile
Users can write personalized introduction to show themselves.

* #####Communication between Users
Users can communicate others by words on the website.