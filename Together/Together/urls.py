from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from dajaxice.core import dajaxice_autodiscover #@UnresolvedImport

dajaxice_autodiscover()


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

if settings.DEBUG:
#    urlpatterns = patterns('',
##        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
##            'document_root': settings.STATIC_ROOT,
##        }),
#        url(r'^static/(?P<path>.*)$', 'MainSite.views.test', {
#            'document_root': settings.STATIC_ROOT,
#        }),
    urlpatterns = static(settings.STATIC_URL)
   

urlpatterns += patterns('',
    # Examples:
    # url(r'^$', 'Together.views.home', name='home'),
    # url(r'^Together/', include('Together.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
    url(r"^", include("MainSite.urls")),
)


