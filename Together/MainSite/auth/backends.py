'''
Created on May 1, 2012  1:58:03 AM

@author:  Kangda Hu
'''

from MainSite.models import User #@UnresolvedImport

class ModelBackend(object):
    """
    Authenticates against MainSite.models.User.
    """
    
    def authenticate(self, email=None, password=None):
        """
        Authenticate username and password.
        """
        try:
            user = User.objects.get(email=email)
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            return None
    
    def get_user(self, user_id):
        """
        Get user obj corresponds to user_id.
        """
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None