'''
Created on May 19, 2012

@author: titi
'''
from django import template
from django.conf import settings

from MainSite.utils.settings import COUNTRIES #@UnresolvedImport


register = template.Library()

@register.simple_tag
def get_fullname_for_country(short):
    """
    Get full country name from the short label.
    
    Syntax::

        {% get_fullname_for_country SHORT %}
        
    Example::

        {% get_fullname_for_country CN %}
    """
    for elem in COUNTRIES:
        if elem[0] == short:
            return elem[1]