'''
Created on Apr 14, 2012  9:04:14 PM

@author:  Kangda Hu
'''
from django.conf.urls import patterns, url

urlpatterns = patterns('MainSite.views',
    url('^(?P<user_id>\d+)/$', 'users.user_detail', name='user-detail'),
    url('^(?P<user_id>\d+)/network/$', 'users.user_network', name='user-network'),
    url('^(?P<user_id>\d+)/favourites/$', 'activities.activity_favourite', name='user-favourite'),
    url('^(?P<user_id>\d+)/joins/$', 'activities.activity_join', name='user-join'),
)