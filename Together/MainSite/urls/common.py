'''
Created on May 3, 2012  12:53:09 PM

@author:  Kangda Hu
'''
from django.conf.urls import patterns, url

urlpatterns = patterns('MainSite.views',
    url('^$', 'index', name='index-page'),
    url('^home/$', 'users.home', name='user-home'),
    url('^signup/$', 'signup', name='sign-up'),
    url('^signin/$', 'signin', name='sign-in'),
    url('^signout/$', 'signout', name="sing-out"),
    url('^network/$', 'users.network', name="network-info"),
    url('^favourites/$', 'activities.activity_favourite', name="favourite-info"),
    url('^joins/$', 'activities.activity_join', name="join-info"),
    url('^status/$', 'users.status', name="status"),
    url('^messages/$', 'messages.message_inbox')
)