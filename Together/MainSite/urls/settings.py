'''
Created on Apr 14, 2012  9:04:01 PM

@author:  Kangda Hu
'''
from django.conf.urls import patterns, url

urlpatterns = patterns('MainSite.views.settings',
    url('^$', 'setting_detail', name='setting-detail'),
    url('^password/$', 'setting_password', name='setting-password'),
    url('^password/password_reset_confirmation$', 'setting_password_confirm', name='setting-password-confirm'),
    url('^notifications/$', 'setting_notification', name='setting-notification'),
)