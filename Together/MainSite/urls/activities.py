'''
Created on Apr 14, 2012  9:04:26 PM

@author:  Kangda Hu
'''
from django.conf.urls import patterns, url

urlpatterns = patterns('MainSite.views.activities',
    url('^$', 'activity_list', name='activity-list'),
    url('^(?P<activity_id>\d+)/$', 'activity_detail', name='activity-detail'),
    url('^(?P<activity_id>\d+)/edit/$', 'activity_edit', name='activity-edit'),
    url('^(?P<activity_id>\d+)/comment/$', 'activity_comment', name='activity-comment'),
    url('^(?P<activity_id>\d+)/delete/$', 'activity_delete', name='activity-delete'),
#    url('^(?P<activity_id>\d+)/vote/$', 'activity_vote', name='activity-vote'),
    url('^new/$', 'activity_new', name='activity-new'),
)