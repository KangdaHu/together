from django.db.models import Q
from datetime import datetime
from django.db import models
from django.utils import timezone
from django.core.mail import send_mail
from django.contrib.auth.hashers import check_password 
from django.contrib.auth.hashers import make_password
from django.core.exceptions import ObjectDoesNotExist

from tagging.fields import TagField #@UnresolvedImport

from MainSite.managers import UserManager #@UnresolvedImport
from MainSite.settings import ACTMETA_VOTE #@UnresolvedImport
from MainSite.settings import ACTMETA_LIKE #@UnresolvedImport
from MainSite.settings import ACTMETA_JOIN #@UnresolvedImport
from wmd.models import MarkDownField #@UnresolvedImport


# Create your models here.

class User(models.Model):
    """
    Model for User
    Email and password is required.
    """
    
    email       = models.EmailField(blank=False)
    nickname    = models.CharField(max_length=30, blank=False)
    password    = models.CharField(max_length=128, blank=False)
    country     = models.CharField(max_length=20, blank=False, default='ZZ')
    city        = models.CharField(max_length=30)
    is_admin    = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    about       = models.TextField()
    last_login  = models.DateTimeField(default=datetime.now)
    join_date   = models.DateTimeField(default=datetime.now)
    
    objects     = UserManager()
    
    class Meta:
        db_table = "mainsite_user"
        ordering = ('id',)
        verbose_name = "user"
        verbose_name_plural = "users"
        
    def __unicode__(self):
        return self.nickname;
    
    def natural_key(self):
        return (self.email,)
    
    def get_absolute_url(self):
        return "/users/%i/" % self.id
    
    @classmethod
    def is_active(self, email=None):
        """
        is_active need to be set by invitation
        """
        if email is None:
            return self.is_verified
        else:
            try:
                return self.objects.get(email=email).is_verified
            except ObjectDoesNotExist:
                return False
    
    def is_anonymous(self):
        """
        Always returns False. This is a way of comparing User objects to
        anonymous users.
        """
        return False
    
    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True
    
    def set_password(self, raw_password):
        """
        Set user's password
        """
        self.password = make_password(raw_password)
        self.save()
    
    def check_password(self, raw_password):
        """
        Check wether the password is correct.
        Return True if it is correct, or it is incorrect.
        """
        def setter(raw_password):
            self.set_password(raw_password)
            self.save()
        return check_password(raw_password, self.password, setter)
    
    def update_last_login(self):
        """
        Update the last login time
        """
        self.last_login = timezone.now()
        self.save()
    
    def email_to_user(self, subject, message, from_email=None):
        """
        Send mail to the user.
        """
        send_mail(subject, message, from_email, [self.email])
    
    @classmethod
    def email_exist(self, email):
        """
        Return True if the email address has been registered, or return False
        """
        return (False, True)[self.objects.filter(email=email).count() is not 0]
    
    @classmethod
    def has_email_existed(self, email):
        """
        Return True if the email address has been registered, or return False
        """
        return bool(self.objects.filter(email=email))
    
    @classmethod
    def get_users_by_ids(cls, ids):    
        """
        Return Message object list according to the id list.
        """
        return cls.objects.filter(id__in=ids)
    
    @classmethod    
    def get_user_by_id(cls, uid):
        """
        Return Message object according to the id.
        """
        try:
            return cls.objects.get(id=uid)
        except cls.DoesNotExist:
            return None

class AnonymousUser(object):
    id = None #@ReservedAssignment
    email       = ''
    nickname    = ''
    is_admin    = False
    is_verified = False

    def __init__(self):
        pass

    def __unicode__(self):
        return 'AnonymousUser'

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __eq__(self, other):
        return isinstance(other, self.__class__)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return 1 # instances always return the same hash value

    def save(self):
        raise NotImplementedError

    def delete(self):
        raise NotImplementedError

    def set_password(self, raw_password):
        raise NotImplementedError

    def check_password(self, raw_password):
        raise NotImplementedError

    def is_anonymous(self):
        return True

    def is_authenticated(self):
        return False


class UserMeta(models.Model):
    """
    Model for user's metadata
    """
    user_id     = models.IntegerField(blank=False)
    meta_key    = models.CharField(max_length=30)
    meta_value  = models.TextField()
    
    objects     = models.Manager()
    
    @classmethod
    def create_meta_data(cls, user_id, meta_key, meta_value):
        
        if not cls.has_meta_value(user_id, meta_key, meta_value):
            newMeta = UserMeta(user_id=user_id,
                               meta_key=meta_key,
                               meta_value=meta_value)
            newMeta.save()
    
    @classmethod
    def delete_meta_data(cls, user_id, meta_key, meta_value):
        
        if cls.has_meta_value(user_id, meta_key, meta_value):
            meta = cls.objects.get(user_id=user_id,
                                   meta_key=meta_key,
                                   meta_value=meta_value)
            meta.delete()
    
    @classmethod
    def get_user_count(cls, meta_key, meta_value):
        """
        Return the count of selected users.
        """
        return UserMeta.objects.filter(meta_key=meta_key,
                                       meta_value=meta_value).count()
    
    @classmethod
    def has_meta_value(cls, user_id, meta_key, meta_value):
        """
        Get activity's meta value.
        """
        return bool(cls.objects.filter(user_id=user_id,
                                       meta_key=meta_key,
                                       meta_value=meta_value))
    
    @classmethod
    def get_metakey_count(cls, user_id, meta_value):
        """
        Get user's meta count according to user_id.
        """
        return UserMeta.objects.filter(user_id=user_id,
                                       meta_value=meta_value).count()
    
    @classmethod
    def get_metakey_list(cls, user_id, meta_value):
        
        keyList = cls.objects.filter(user_id=user_id,
                                     meta_value=meta_value).values('meta_key')
        return [ item['meta_key'] for item in keyList ]
    
    @classmethod
    def get_user_list(cls, meta_key, meta_value):
        """
        Return the id list of selected users.
        """
        return UserMeta.objects.filter(meta_key=meta_key,
                                       meta_value=meta_value).values('user_id')
    
    class Meta:
        db_table = "mainsite_usermeta"
        verbose_name = "usermeta"
        verbose_name_plural = "usermeta"

class Activity(models.Model):
    """
    Model for activities
    """
    
    title       = models.CharField(max_length=255)
    creator_id  = models.IntegerField()
    url         = models.URLField() # activity's site
    news        = models.TextField()
    content     = MarkDownField()
    is_alone    = models.BooleanField() #only the creator can participate
    location    = models.CharField(max_length=255) #activity's location
    creat_time  = models.DateTimeField(default=datetime.now) #the time of activity's creation
    start_time  = models.DateTimeField() #activity start time
    has_endtime = models.BooleanField() 
    end_time    = models.DateTimeField(blank=True) #activity end time
    vote        = models.IntegerField(default=0)
    popular     = models.IntegerField(default=0)
    tags        = TagField()
    
    objects     = models.Manager()
    
    def get_creator(self):
        """
        Get creator user object
        """
        return User.get_user_by_id(self.creator_id)
    
    def get_absolute_url(self):
        """
        Return absolute url of activity
        """
        return "/activities/{0}/".format(self.id)
    
    @classmethod
    def is_alone_activity(self, aid):
        
        return Activity.objects.get(id=aid).is_alone
    
    @classmethod
    def get_activities_by_ids(cls, ids):    
        """
        Return Message object list according to the id list.
        """
        return cls.objects.filter(id__in=ids).order_by('-creat_time')
    
    @classmethod
    def get_activity_by_id(cls, aid):
        
        try:
            return cls.objects.get(id=aid)
        except cls.DoesNotExist:
            return None
    
    class Meta:
        """
        Activity's Meta
        """
        db_table = "mainsite_activity"
        ordering = ['-creat_time']
        get_latest_by = 'creat_time'
        verbose_name = 'activity'
        verbose_name_plural = 'activities'

class ActivityMeta(models.Model):
    """
    Model for activity's metadata
    """
    activity_id = models.IntegerField(blank=False)
    meta_key    = models.CharField(max_length=30)
    meta_value  = models.TextField()
    
    objects     = models.Manager()
    
    @classmethod
    def create_meta_data(cls, activity_id, meta_key, meta_value):
        
        if not cls.has_meta_value(activity_id, meta_key, meta_value):
            newMeta = ActivityMeta(activity_id=activity_id,
                                   meta_key=meta_key,
                                   meta_value=meta_value)
            newMeta.save()
    
    @classmethod
    def delete_meta_data(cls, activity_id, meta_key, meta_value):
        
        if cls.has_meta_value(activity_id, meta_key, meta_value):
            meta = cls.objects.get(activity_id=activity_id,
                                   meta_key=meta_key,
                                   meta_value=meta_value)
            meta.delete()
    
    @classmethod
    def get_activity_count(cls, meta_key, meta_value):
        """
        Get activity's meta count.
        """
        return cls.objects.filter(meta_key=meta_key,
                                  meta_value=meta_value).count()
    
    @classmethod
    def has_meta_value(cls, activityId, meta_key, meta_value):
        """
        Get activity's meta value.
        """
        return bool(cls.objects.filter(activity_id=activityId,
                                       meta_key=meta_key,
                                       meta_value=meta_value))
    
    @classmethod
    def get_metakey_count(self, activityId, meta_value):
        """
        Return the count of selected users.
        """
        return self.objects.filter(activity_id=activityId,
                                   meta_value=meta_value).count()
    
    @classmethod
    def get_metakey_list(cls, activity_id, meta_value):
        
        keyList = cls.objects.filter(activity_id=activity_id,
                                    meta_value=meta_value).values('meta_key')
        return [ item['meta_key'] for item in keyList ]
    
    @classmethod
    def get_activity_list(cls, meta_key, meta_value):
        """
        Return the id list of selected users.
        """
        idList = cls.objects.filter(meta_key=meta_key,
                                    meta_value=meta_value).values('activity_id')
        return [ item['activity_id'] for item in idList ]
    
    class Meta:
        db_table = "mainsite_activitymeta"
        verbose_name = "activitymeta"
        verbose_name_plural = "activitymeta"
    
class Comment(models.Model):
    """
    Model for comments
    """
#    author_id = models.ForeignKey("User", to_field="id", on_delete=False, 
#                                  verbose_name="comment_author_id")
#    activity_id = models.ForeignKey("Activity", to_field="id", on_delete=False, 
#                                    verbose_name="comment_activity_id")
    author_id   = models.IntegerField()
    activity_id = models.IntegerField()
    submit_date = models.DateTimeField()
    content     = models.TextField()
    #agent = models.CharField("comment_agent", max_length=200)
    #author_ip = models.IPAddressField("comment_author_ip")
    parent      = models.IntegerField()
    
    objects     = models.Manager()
    
    class Meta:
        db_table = "mainsite_comment"
        ordering = ('-submit_date',)
        verbose_name = 'comment'
        verbose_name_plural = 'comments'
    
    def get_author(self):
        """
        Return author object
        """
        
#        return User.objects.get(id=self.author_id)
        return User.get_user_by_id(self.author_id)
    
    def get_activity(self):
        """
        Return author object
        """
        return Activity.get_activity_by_id(self.activity_id)
    
    def get_formated_time(self):
        """
        Return the formated time
        """
        return self.submit_date.strftime("%a %b %d %X %Y")
    
    @classmethod
    def get_comments_by_ids(cls, ids):    
        """
        Return Message object list according to the id list.
        """
        return cls.objects.filter(id__in=ids).order_by('-submit_date')
    
    @classmethod    
    def get_comment_by_id(cls, cid):
        """
        Return Message object according to the id.
        """
        try:
            return cls.objects.get(id=cid)
        except cls.DoesNotExist:
            return None
    
  
class Message(models.Model):
    """
    Model for comments
    """
#    author_id = models.ForeignKey("User", to_field="id", on_delete=False, 
#                                  verbose_name="comment_author_id")
#    activity_id = models.ForeignKey("Activity", to_field="id", on_delete=False, 
#                                    verbose_name="comment_activity_id")
    from_id     = models.IntegerField()
    to_id       = models.IntegerField()
    read_status = models.BooleanField(default=0)
    send_date   = models.DateTimeField(default=datetime.now)
    content     = models.TextField()
    #agent = models.CharField("comment_agent", max_length=200)
    #author_ip = models.IPAddressField("comment_author_ip")
    
    objects     = models.Manager()
    
    class Meta:
        db_table = "mainsite_message"
        ordering = ('-send_date',)
        verbose_name = 'message'
        verbose_name_plural = 'messages'
    
    @classmethod
    def create_message(cls, fid, tid, content):
        new_msg = Message(from_id=fid, to_id=tid, content=content)
        new_msg.save()
    
    @classmethod
    def delete_meta_data(cls, mid):
        
        msg = cls.objects.get(id=mid)
        msg.delete()
    
    @classmethod
    def get_contact_list(cls, uid):
        """
        Return the Contact List of the user with id.
        """
        msgList = cls.objects.filter(Q(from_id=uid) | Q(to_id=uid))
        sender = set([ item['from_id'] for item in msgList.values('from_id')])
        receiver = set([ item['to_id'] for item in msgList.values('to_id')])
        contact = sender | receiver
        contact.remove(uid)
        
        return list(contact)
        
    
    @classmethod
    def get_latest_message_by_from_and_to(cls, fid, tid):
        """
        Return the id of latest message from or to user with id.
        """
        return cls.objects.filter(Q(from_id=fid, to_id=tid) | \
                                  Q(from_id=tid, to_id=fid))\
                                  .order_by('-send_date')[0]
    
    @classmethod
    def get_message_count_by_from_and_to(cls, fid, tid):
        """
        Return the id list of the history of message between from and to.
        """
        return cls.objects.filter(Q(from_id=fid, to_id=tid) | \
                                  Q(from_id=tid, to_id=fid)).count()
    
    @classmethod
    def get_message_list_by_from_and_to(cls, fid, tid, start=None, end=None):
        """
        Return the id list of the history of message between from and to.
        """
        return cls.objects.filter(Q(from_id=fid, to_id=tid) |\
                                  Q(from_id=tid, to_id=fid))\
                                  .order_by('-send_date')[start:end]
    
    @classmethod
    def get_messages_by_ids(cls, ids):    
        """
        Return Message object list according to the id list.
        """
        return Message.objects.filter(id__in=ids).order_by('-send_date')
    
    @classmethod    
    def get_message_by_id(cls, mid):
        """
        Return Message object according to the id.
        """
        try:
            return Message.objects.get(id=mid)
        except Message.DoesNotExist:
            return None
        
    @classmethod
    def has_unread_message(cls, fid, sid=None):
        """
        Return whether fid(or between fid and sid) has unread message.
        
        ATTENTION:fid MUST BE current user's id
        """
        if sid is None:
            return bool(cls.objects.filter(Q(to_id=fid)&Q(read_status=0)))
        else:
            #Just test received message
            return bool(cls.objects.filter(from_id=sid, to_id=fid, read_status=0))
    
    @classmethod
    def mark_message(cls, fid, tid):
        """
        Mark the message status read between fid and tid.
        """
        cls.objects.filter(from_id=tid, to_id=fid, read_status=0).update(read_status=1)
    