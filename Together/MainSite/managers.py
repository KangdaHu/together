'''
Created on Apr 14, 2012  11:57:44 PM

@author:  Kangda Hu
'''
from django.db import models
from django.utils import timezone

class UserManager(models.Manager):
    """
    Manager for User model
    """
    @classmethod
    def normalize_email(cls, email):
        """
        Normalize the address by lowercasing the domain part of the email
        address.
        """
        email = email or ''
        try:
            email_name, domain_part = email.strip().rsplit('@', 1)
        except ValueError:
            pass
        else:
            email = '@'.join([email_name, domain_part.lower()])
        return email
    
    def create_user(self, email, password, nickname=None):
        """
        Create a User with the given email nickname and password
        """
        if not email:
            raise ValueError("The email must be set.")
        elif not password:
            raise ValueError("The password must be set")
        
        email = UserManager.normalize_email(email)
        
        if not nickname:
            nickname = email
        
        now = timezone.now()
        user = self.model(email=email, nickname=nickname,
                          is_verified=False, join_date=now)
        user.set_password(password)
        user.save(using=self._db)
        
        return user
    
    def create_administrator(self, email, password, nickname):
        u = self.create_user(email, password, nickname)
        u.is_admin = True
        u.save(using=self._db)
        return u
    
    def get_by_natural_key(self, email):
        return self.get(email=email)
