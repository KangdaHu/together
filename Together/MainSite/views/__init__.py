from django.http import HttpResponse, HttpResponseRedirect
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from django.forms.forms import ErrorList

from Together.settings import STATIC_URL #@UnresolvedImport
from MainSite.models import User #@UnresolvedImport
from MainSite.utils.form import SigninForm #@UnresolvedImport
from MainSite.utils.form import SignupForm #@UnresolvedImport
from MainSite.auth import authenticate, login, logout #@UnresolvedImport
from MainSite.auth import get_user #@UnresolvedImport


def index(request):
    """
    View for index page.
    """
    #return HttpResponse("Hello world")
    
    user = get_user(request)
    if user is None or user.is_anonymous():
        is_logged = False
    else:
        return HttpResponseRedirect('/home/')
    
    inform = SigninForm()
    upform = SignupForm()
    
    context = RequestContext(request, 
                    {'STATIC_URL':STATIC_URL, 'is_logged':is_logged,
                     'inform':inform, 'upform':upform, })
    return render_to_response("common/index.html", context)

def signin(request):
    """
    View for sign in page.
    """
    user = get_user(request)
    if user is None or user.is_anonymous():
        is_logged = False
    else:
        return HttpResponseRedirect('/home/')
    
    if request.method == 'POST':
        inform = SigninForm(request.POST)
        
        is_valid = True
        if not inform.is_valid():
            is_valid = False
        
        try:
            if not User.has_email_existed(inform.cleaned_data['email']):
                #email not registered.
                is_valid = False
                el = ErrorList()
                el.append("The Email address has not been Registered.")
                inform.errors['email'] = el
            else:
                if not User.is_active(inform.cleaned_data['email']):
                    #email not confirmed.
                    is_valid = False
                    el = ErrorList()
                    el.append("The Email address has not been Confirmed.")
                    inform.errors['email'] = el
        except AttributeError:
            pass
        
        if is_valid:
            #login
            user = authenticate(email=inform.cleaned_data['email'],
                                password=inform.cleaned_data['password'])
            if user is not None:
                login(request, user)
                if not inform.cleaned_data['remember']:
                    request.session.set_expiry(0)
                return HttpResponseRedirect('/home/')
            else:
                el = ErrorList()
                el.append("Incorrect Password!")
                inform.errors['password'] = el
        
    else:
        inform = SigninForm()
    
    context = RequestContext(request, 
                    {'STATIC_URL':STATIC_URL, 'is_logged':is_logged,
                     'inform' : inform, })
    return render_to_response("common/signin.html", context)

def signout(request):
    """
    Logout.
    """
    logout(request)
    return HttpResponseRedirect('/')

def signup(request):
    """
    View for sign up page.
    """
    user = get_user(request)
    if user is None or user.is_anonymous():
        is_logged = False
    else:
        return HttpResponseRedirect('/home/')
    
    inform = SigninForm()
    
    if request.method == 'POST':
        upform = SignupForm(request.POST)
        
        is_valid = True  
        if not upform.is_valid():
            is_valid = False
        try:
            if User.has_email_existed(upform.cleaned_data['email']):
                #email has existed.
                is_valid = False
                el = ErrorList()
                el.append("The Email Address has existed.")
                upform.errors['email'] = el
            
            if upform.cleaned_data['password'] != upform.cleaned_data['confirm_password']:
                #email has existed.
                is_valid = False
                el = ErrorList()
                el.append("Confirm password and password is not the same.")
                upform.errors['confirm_password'] = el
        except AttributeError:
            pass
        
        if is_valid:
            #create new user here!
            User.objects.create_user(upform.cleaned_data['email'],
                                     upform.cleaned_data['password'],
                                     upform.cleaned_data['nickname'])
            
            #send confirm email
            
            context = RequestContext(request, {'STATIC_URL': STATIC_URL,
                                               'is_logged' : False,
                                               'confirm_notify' : True,
                                               'confirm_email' : upform.cleaned_data['email'],
                                               'inform' : inform,})
            return render_to_response("common/signup.html", context)

    else:
        upform = SignupForm()
    
    context = RequestContext(request, {'STATIC_URL': STATIC_URL,
                                       'is_logged' : is_logged,
                                       'confirm_notify' : False,
                                       'inform' : inform,
                                       'upform' : upform, })
    return render_to_response("common/signup.html", context)


