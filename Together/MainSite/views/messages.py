'''
Created on Jul 14, 2012

@author: titi
'''
from django.http import HttpResponseRedirect
from django.template.context import RequestContext
from django.shortcuts import render_to_response

from Together.settings import STATIC_URL #@UnresolvedImport
from MainSite.models import User #@UnresolvedImport
from MainSite.models import Message #@UnresolvedImport
from MainSite.models import UserMeta #@UnresolvedImport
from MainSite.settings import COMMENTS_PER_PAGE as per #@UnresolvedImport
from MainSite.utils import check_user #@UnresolvedImport
from MainSite.utils.form import MessageForm #@UnresolvedImport
from MainSite.settings import USERMETA_FOLLOW #@UnresolvedImport


def message_inbox(request):
    """
    View for message list.
    """
    (response, user, is_logged) = check_user(request, '/signin/')
    if response is not None:
        return response
    
    if request.GET:
        return message_detail(request, user)
    
    if request.POST:
        return message_new(request, user)
    
    contacts = get_contact_list(user)
    
    context = RequestContext(request,
              {'STATIC_URL':STATIC_URL, 'is_home':True,
               'is_logged':is_logged, 'logged_user':user, 'show_user':user,
               'has_new_message':Message.has_unread_message(user.id),
               'user_followers' : UserMeta.get_user_count(user.id, USERMETA_FOLLOW),
               'user_following' : UserMeta.get_metakey_count(user.id, USERMETA_FOLLOW),
               'contacts' : contacts,
               'msgForm' : get_message_form(user),
               })
    return render_to_response("message/message_contact.html", context)

def message_detail(request, fuser):
    """
    View for message detail.
    """
    
    tidStr = request.GET.get('tid')
    
    if tidStr is None:
        return HttpResponseRedirect("/home/")
    else:
        tid = int(tidStr)
    
    msgs = Message.get_message_list_by_from_and_to(fuser.id, tid, 0, 20)
    
    messages = []
    for msg in msgs:
        messages.append((User.get_user_by_id(msg.from_id), msg))
        
    Message.mark_message(fuser.id, tid)
    
    context = RequestContext(request,
              {'STATIC_URL':STATIC_URL, 'is_home':True,
               'is_logged':True, 'logged_user':fuser,
               'has_new_message':Message.has_unread_message(fuser.id),
               'show_user':fuser,
               'user_followers':UserMeta.get_user_count(fuser.id, USERMETA_FOLLOW),
               'user_following':UserMeta.get_metakey_count(fuser.id, USERMETA_FOLLOW),
               'contact':User.get_user_by_id(tid),
               'messages':messages,
               'msgForm':get_message_form(user=fuser, initial={'recipient':tid}),
               })
    return render_to_response("message/message_detail.html", context)

def message_new(request, user):
    """
    Handler for new message.
    """
    msg_form = get_message_form(user=user, request=request)
    
    if msg_form.is_valid():
        
        tid = msg_form.cleaned_data['recipient']
        content = msg_form.cleaned_data['content']
        Message.create_message(user.id, tid, content)
        
        return HttpResponseRedirect("/messages/")
    else:
        error = True
    
    context = RequestContext(request,
              {'STATIC_URL':STATIC_URL, 'is_home':True,
               'is_logged':True, 'logged_user':user,'show_user':user,
               'has_new_message':Message.has_unread_message(user.id),
               'user_followers':UserMeta.get_user_count(user.id, USERMETA_FOLLOW),
               'user_following':UserMeta.get_metakey_count(user.id, USERMETA_FOLLOW),
               'contacts':get_contact_list(user),
               'msgForm':msg_form,
               'msg_form_error':error,
               })
    return render_to_response("message/message_contact.html", context)

def get_message_form(user, request=None, initial=None):  
    """
    Return the form object with choices having been set up.
    """
    
    contact_list = User.get_users_by_ids(UserMeta.get_user_list(user.id, USERMETA_FOLLOW))
    choices = [("","")]
    for contact in contact_list:
        choices.append((contact.id, '{0.nickname}({0.email})'.format(contact)))
    
    if request is None:
        msg_form = MessageForm(initial=initial)
    else:
        msg_form = MessageForm(request.POST)
        
    msg_form.set_recipients(choices)
    
    return msg_form

def get_contact_list(user):
    """
    Return contact object list according to user.
    """
    
    contactList = User.get_users_by_ids(Message.get_contact_list(user.id))
    contacts = []
    for contact in contactList:
        msg = Message.get_latest_message_by_from_and_to(user.id, contact.id)
        count = Message.get_message_count_by_from_and_to(user.id, contact.id)
        has_new = Message.has_unread_message(user.id, contact.id)
        contacts.append((contact, msg, count, has_new))
    
    return contacts

