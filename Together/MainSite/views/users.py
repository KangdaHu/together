'''
Created on Apr 14, 2012  11:33:24 PM

@author:  Kangda Hu
'''

from datetime import datetime

from django.http import HttpResponseRedirect
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from django.db.models import Q

from Together.settings import STATIC_URL #@UnresolvedImport
from MainSite.auth import get_user #@UnresolvedImport
from MainSite.settings import COMMENTS_PER_PAGE as per #@UnresolvedImport
from MainSite.models import User  #@UnresolvedImport
from MainSite.models import ActivityMeta #@UnresolvedImport
from MainSite.models import UserMeta #@UnresolvedImport
from MainSite.models import Comment #@UnresolvedImport
from MainSite.models import Message #@UnresolvedImport
from MainSite.utils import check_user #@UnresolvedImport
from MainSite.utils.form import StatusForm #@UnresolvedImport
from MainSite.settings import ACTMETA_LIKE  #@UnresolvedImport
from MainSite.settings import ACTMETA_JOIN #@UnresolvedImport
from MainSite.settings import USERMETA_FOLLOW #@UnresolvedImport


def home(request):
    """
    View for user home page.
    """
    #return HttpResponse("Hello world")
    
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        return response
    
    return network(request)

def network(request):
    """
    View for network info
    """
    user = get_user(request)
    return user_network(request, user.id, "user/home.html")

def user_detail(request, user_id):
    
    (response, luser, is_logged) = check_user(request, '/signin/')
    if response is not None:
        return response
    
    suser = User.objects.get(id=user_id)
    
    context = RequestContext(request,
              {'STATIC_URL':STATIC_URL,
               'is_home':True,
               'is_logged':is_logged,
               'is_profile':True,
               'has_new_message':Message.has_unread_message(luser.id),
               'logged_user':luser,
               'show_user':suser,
               'user_followers':UserMeta.get_user_count(suser.id, USERMETA_FOLLOW),
               'user_following':UserMeta.get_metakey_count(suser.id, USERMETA_FOLLOW),
               'user_like':ActivityMeta.get_activity_count(suser.id, ACTMETA_LIKE),
               'user_in':ActivityMeta.get_activity_count(suser.id, ACTMETA_JOIN),
               'follow_state':UserMeta.has_meta_value(luser.id, suser.id, USERMETA_FOLLOW),
               'latest_activity_title' : "Taiwan Tour",
               'latest_activity_creator' : "Titi",
               'latest_activity_detail' : r"Just a tour to Taiwan."
               })
    return render_to_response("user/common_user.html", context)
    
def user_network(request, user_id, template=None):
    """
    View for network info
    """
    (response, luser, is_logged) = check_user(request, '/signin/')
    if response is not None:
        return response

###TOFIX!!!Code Below MAY Cause BUG!!!
    try: 
        suser = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return HttpResponseRedirect('/signin/')
###TOFIX!!!

    (cmt_list, total_page, cur_page) = get_user_network(request, suser.id)
    
    context = RequestContext(request,
              {'STATIC_URL': STATIC_URL,
               'is_home' : True,
               'is_logged' : is_logged,
               'is_network' : True,
               'has_new_message':Message.has_unread_message(luser.id),
               'logged_user' : luser,
               'show_user' : suser,
               'cmt_list' : cmt_list,
               'cur_page' : cur_page,
               'total_page' : total_page,
               'user_followers' : UserMeta.get_user_count(suser.id, USERMETA_FOLLOW),
               'user_following' : UserMeta.get_metakey_count(suser.id, USERMETA_FOLLOW),
               'user_like' : ActivityMeta.get_activity_count(suser.id, ACTMETA_LIKE),
               'user_in' : ActivityMeta.get_activity_count(suser.id, ACTMETA_JOIN),
               'latest_activity_title' : "Taiwan Tour",
               'latest_activity_creator' : "Titi",
               'latest_activity_detail' : r"Just a tour to Taiwan.",
               })
    if template is None:
        return render_to_response("user/common_user.html", context)
    else:
        return render_to_response(template, context)

def get_user_network(request, user_id):
    
#    act_ids = UserMeta.objects.filter(user_id=user_id, meta_key=ACTMETA_LIKE).values("meta_value")
    act_ids = ActivityMeta.get_activity_list(user_id, ACTMETA_LIKE)
    cmts = Comment.objects.filter(Q(author_id=user_id) | #comment by user
                                  Q(activity_id__in=act_ids) #comment on favourite activity
                                  ).order_by('-submit_date')
    cmt_num = cmts.count()
    
    total_page = (cmt_num / per, cmt_num / per + 1)[cmt_num % per != 0];
    total_page = (1, total_page)[total_page is not 0]
    if request.method == 'GET':
        cur_page = int(request.GET.get('page', '1'))
    else:
        cur_page = 1
    cur_page = (cur_page, total_page)[cur_page > total_page]
    
    cmt_start = (cur_page - 1) * per
    cmt_end = (cur_page * per, cmt_num)[cur_page * per > cmt_num]
    
    cmt_list = []
    for cmt in cmts[cmt_start:cmt_end]:
        cmt_list.append((cmt, cmt.get_author(), cmt.get_activity()))
        
    return (cmt_list, total_page, cur_page)
 
def status(request):
    """
    Handler for tweets.
    """
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        return response
    
    if request.method == 'POST':
        staform = StatusForm(request.POST)
        if staform.is_valid():
            status = Comment(
                        author_id=user.id,
                        activity_id=-1,
                        submit_date=datetime.now(),
                        content=staform.cleaned_data['content'],
                        parent=-1)
            status.save()
    
    return HttpResponseRedirect("/home/")
    

