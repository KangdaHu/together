'''
Created on Apr 14, 2012  11:34:56 PM

@author:  Kangda Hu
'''
from django.http import HttpResponseRedirect
from django.http import Http404 
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from datetime import datetime

from Together.settings import STATIC_URL #@UnresolvedImport
from MainSite.models import User #@UnresolvedImport
from MainSite.models import Activity #@UnresolvedImport
from MainSite.models import Comment #@UnresolvedImport
from MainSite.models import ActivityMeta #@UnresolvedImport
from MainSite.models import Message #@UnresolvedImport
from MainSite.settings import COMMENTS_PER_PAGE as per #@UnresolvedImport
from MainSite.utils.form import SigninForm #@UnresolvedImport
from MainSite.utils.form import CommentForm #@UnresolvedImport
from MainSite.utils.form import ActivityForm #@UnresolvedImport
from MainSite.utils.form import VoteForm #@UnresolvedImport
from MainSite.utils import check_user #@UnresolvedImport
from MainSite.utils import error_page #@UnresolvedImport
from MainSite.settings import ACTMETA_VOTE #@UnresolvedImport
from MainSite.settings import ACTMETA_LIKE #@UnresolvedImport
from MainSite.settings import ACTMETA_JOIN #@UnresolvedImport

def activity_list(request, alert = None):
    """
    View for activity list
    """
    #test user's cookie
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    
    act_start = 0
    act_end = 20
    
    if request.method == "GET":
        order = request.GET.get('order', 'latest')
        if order == 'latest':
            acts = Activity.objects.all()[act_start:act_end]
        elif order == 'popular':
            acts = Activity.objects.order_by('-popular')[act_start:act_end]
        elif order == 'vote':
            acts = Activity.objects.order_by('-vote')[act_start:act_end]
        elif order == 'ongo':
            acts = Activity.objects.filter(start_time__lte=datetime.now)[act_start:act_end]
        elif order == 'notyet':
            acts = Activity.objects.filter(start_time__gt=datetime.now)[act_start:act_end]    
    else:
        order = 'latest'
        acts = Activity.objects.all()[act_start:act_end]
    
    act_list = []
    for act in acts:
        act_list.append((act,act.get_creator()))
    
    inform = SigninForm()
    
    context = RequestContext(request, 
                             {'STATIC_URL': STATIC_URL, 'is_act' : True,
                              'is_logged' : is_logged,
                              'has_new_message':Message.has_unread_message(user.id),
                              'logged_user' : user, 'activity_list' : act_list,
                              'inform' : inform, 'order' : order,
                              'alert' : alert,})
    return render_to_response("activity/activities.html", context)

def activity_detail(request, activity_id, cmtform = None):
    """
    View for activity's detail.
    """
    #test user's cookie
#    user = get_user(request)
#    if user is None or user.is_anonymous():
#        return HttpResponseRedirect('/signin/')
#        is_logged = False
#    else:
#        is_logged = True
    
    (response, user, is_logged) = check_user(request, '/signin/')
    if response is not None:
        return response
    
    inform = SigninForm()
    try:
        act = Activity.objects.get(id=activity_id)
    except:
        raise Http404
    
    (cmt_list, total_page, cur_page) = get_activity_comment(request, activity_id)
    
    show_error = True
    if cmtform is None:
        show_error = False
        cmtform = CommentForm({'author':user.id, 'activity':activity_id})
        
    context = RequestContext(request, 
                             {'STATIC_URL':STATIC_URL, 'is_act':True,
                              'is_logged':is_logged,
                              'has_new_message':Message.has_unread_message(user.id), 
                              'logged_user':user, 'show_activity':act, 'inform':inform,
                              'cmt_list':cmt_list, 'total_page':total_page,
                              'cur_page':cur_page, 'show_error':show_error,
                              'act_address' : '/activities/%s/'%(activity_id),
                              'cmtform':cmtform,
                              'members':User.get_users_by_ids(ActivityMeta.get_metakey_list(act.id, ACTMETA_JOIN)),
                              'voters':get_voter(activity_id),
                              'is_fav':is_favourite(user.id,act.id),
                            })
    return render_to_response("activity/activity_detail.html", context)

def get_activity_comment(request, activity_id):
    """
    For simplifying activity_detail()
    """
    cmts = Comment.objects.filter(activity_id=activity_id).order_by('-submit_date')
    cmt_num = cmts.count()
    
    total_page = (cmt_num / per, cmt_num / per + 1)[cmt_num % per != 0];
    if request.method == 'GET':
        cur_page = int(request.GET.get('page', '1'))
    else:
        cur_page = 1
    cur_page = (cur_page, total_page)[cur_page > total_page]
    
    cmt_start = ((cur_page - 1) * per, 0)[(cur_page - 1) * per < 0]
    cmt_end = (cur_page * per, cmt_num)[cur_page * per > cmt_num]
    
    cmt_list = []
    for cmt in cmts[cmt_start:cmt_end]:
        cmt_list.append((cmt, cmt.get_author()))
        
    return (cmt_list, total_page, cur_page)

def get_voter(activity_id):
    """
    Get the list of voter who vote for the activity assigned with the activity_is.
    """
    
#    ids = UserMeta.objects.filter(meta_value=activity_id,
#                                  meta_key=ACTMETA_VOTE).values('user_id')
    ids = ActivityMeta.get_metakey_list(activity_id, ACTMETA_VOTE)
    voters = User.objects.filter(id__in=ids)
    
    return voters

def is_favourite(user_id, activity_id):
    """
    Return True if activity is the user's favourite, False for not.
    """
#    if UserMeta.get_meta_count(user_id, ACTMETA_LIKE, activity_id) == 0:
#        return False
#    else:
#        return True
    return ActivityMeta.has_meta_value(activity_id, user_id, ACTMETA_LIKE)

def activity_comment(request, activity_id):
    """
    Handler for posting comments.
    """
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        return response
    
    if request.method == 'POST':
        cmtform = CommentForm(request.POST)
        
        if cmtform.is_valid():
            newComment = Comment(
                            author_id=cmtform.cleaned_data['author'],
                            activity_id=cmtform.cleaned_data['activity'],
                            submit_date=datetime.now(),
                            content=cmtform.cleaned_data['content'],
                            parent=-1)
            try:
                newComment.save()
            except:
                #Error occurs while save...
                
                return error_page(request, 
                                  r"Error Occurs",
                                  "Something wrong happened while saving new comment,\
                                  Please contact administrator.")

            #Post successfully!
            return HttpResponseRedirect("/activities/%s/"%(activity_id))
    else:
        return HttpResponseRedirect("/activities/%s/"%(activity_id))

def has_joined(user_id, activity_id):
    """
    Return True if user has joined the activity, or False.
    """
#    if UserMeta.get_meta_count(user_id, ACTMETA_JOIN, activity_id) == 0:
#        return False
#    else:
#        return True
    return ActivityMeta.has_meta_value(activity_id, user_id, ACTMETA_JOIN)


def vote(user_id, activity_id):
    ##Update user's metadata##
#    newMetaVote = ActivityMeta(user_id=user_id,
#                               meta_key=ACTMETA_VOTE,
#                               meta_value=activity_id)
#    try:
#        newMetaVote.save()
#    except:
#        #Error occurs while save...
#        return "Fail"
#    return "Success"
    ActivityMeta.create_meta_data(activity_id, user_id, ACTMETA_VOTE)
    return "Success"

def has_voted(user_id, activity_id):
    """
    Return True if user has voted for the activity, or False.
    """
#    if UserMeta.get_meta_count(user_id, ACTMETA_VOTE, activity_id) == 0:
#        return False
#    else:
#        return True
    return ActivityMeta.has_meta_value(activity_id, user_id, ACTMETA_VOTE)
    
    
def update_vote(activity_id):
    try:
        act = Activity.objects.get(id=activity_id)
    except:
        pass
#        return error_page(request,
#                          r"Error Occurs",
#                          "Something wrong happened while getting activity data,\
#                           Please contact administrator.")
#    act.vote = UserMeta.objects.filter(meta_key=ACTMETA_VOTE,
#                                       meta_value=activity_id).count()
    act.vote = ActivityMeta.get_metakey_count(activity_id, ACTMETA_VOTE)
    
    try:
        act.save()
    except:
        pass
#        return error_page(request,
#                          r"Error Occurs",
#                          "Something wrong happened while updating vote count,\
#                           Please contact administrator.")
    return act.vote

def modify_favourite(user_id, activity_id, action):
    """
    Add Like if action is UP, minus Like if DOWN
    """
    if action == 'UP':
#        newMetaLike = UserMeta(user_id=user_id,
#                               meta_key=ACTMETA_LIKE,
#                               meta_value=activity_id)
#        try:
#            newMetaLike.save()
#        except:
#            return "Fail"
        ActivityMeta.create_meta_data(activity_id, user_id, ACTMETA_LIKE)
        return "Success"
    
    if action == 'DOWN':
        try:
            oldMetaLike = ActivityMeta.objects.get(activity_id=activity_id,
                                                   meta_key=user_id,
                                                   meta_value=ACTMETA_LIKE)
            oldMetaLike.delete()
        except:
            return "Fail"
        return "Success"
    return "Unknown Action"

def update_popular(activity_id):
    try:
        act = Activity.objects.get(id=activity_id)
    except:
        pass
#        return error_page(request,
#                          r"Error Occurs",
#                          "Something wrong happened while getting activity data,\
#                           Please contact administrator.")
#    act.popular = UserMeta.objects.filter(meta_key=ACTMETA_LIKE,
#                                          meta_value=activity_id).count()
    act.popular = ActivityMeta.get_metakey_count(activity_id, ACTMETA_LIKE)
    
    try:
        act.save()
    except:
        pass
#        return error_page(request,
#                          r"Error Occurs",
#                          "Something wrong happened while updating vote count,\
#                           Please contact administrator.")
    return act.popular

def activity_new(request):
    """
    View for new activity.
    """
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        return response
    
    if request.method == 'POST':
        actform = ActivityForm(request.POST)
        
        if actform.is_valid():
            #Create new activity
            act = Activity(
            title       = actform.cleaned_data['title'],
            creator_id  = user.id,
            url         = actform.cleaned_data['url'],
            news        = actform.cleaned_data['news'],
            content     = actform.cleaned_data['content'],
            is_alone    = actform.cleaned_data['is_alone'],
            location    = actform.cleaned_data['location'],
            has_endtime = actform.cleaned_data['has_endtime'],
            start_time  = actform.cleaned_data['start_time'],
            end_time    = actform.cleaned_data['end_time'],
            tags        = actform.cleaned_data['tags'],
            )
            
            act.save()
            
            #Add creator into group. 
            ActivityMeta.create_meta_data(act.id, user.id, ACTMETA_JOIN)

            return error_page(request, 
                              r'Post Successfully!',
                              'Go to <a href="/activities/{0}/">Your Activity</a>,or <a href="/activities/new/">Post Another</a>?.'.format(act.id))
             
    else:
        actform = ActivityForm()
    
    context = RequestContext(request, 
                             {'STATIC_URL':STATIC_URL, 'is_act':True,
                              'is_logged' : is_logged,
                              'has_new_message':Message.has_unread_message(user.id),
                              'logged_user':user, 'action':"/activities/new/",
                              'actform':actform})
    return render_to_response("activity/activity_new.html", context)


def activity_edit(request, activity_id):
    """
    Edit the content of activity.
    """
    (response, user, is_logged) = check_user(request, '/signin/')
    if response is not None:
        return response
    
    is_saved = False
    tips = ""   
    
    try:
        act = Activity.objects.get(id=activity_id)
    except Activity.DoesNotExist:
        tips = "Can't find the Activity object."
        return error_page(request, 
                          r"Error Occurs",
                          "Can't find the Activity object.")
    else:
    
        if user.id != act.creator_id:
            return HttpResponseRedirect("/activities/%s/" % (activity_id))
     
        if request.method == 'POST':
            actform = ActivityForm(request.POST)
        
            if actform.is_valid():
                act.title = actform.cleaned_data['title']
                act.creator_id = user.id
                act.url = actform.cleaned_data['url']
                act.news = actform.cleaned_data['news']
                act.content = actform.cleaned_data['content']
                act.is_alone = actform.cleaned_data['is_alone']
                act.location = actform.cleaned_data['location']
                act.has_endtime = actform.cleaned_data['has_endtime']
                act.start_time = actform.cleaned_data['start_time']
                act.end_time = actform.cleaned_data['end_time']
                act.tags = actform.cleaned_data['tags']
            
                try:
                    user.save()
                except:
                    tips = "Error Ocurr!"
                else:
                    tips = "Save Successfully!"
                    is_saved = True
            
        else:
            profile_data = {
            'title'     : act.title,
            'creator_id': user.id,
            'url'       : act.url,
            'news'      : act.news,
            'content'   : act.content,
            'is_alone'  : act.is_alone,
            'location'  : act.location,
            'has_endtime': act.has_endtime,
            'start_time': act.start_time,
            'end_time'  : act.end_time,
            'tags'      : act.tags,
            }
            actform = ActivityForm(profile_data)
    
    context = RequestContext(request, 
                             {'STATIC_URL':STATIC_URL, 'is_logged':is_logged,
                              'has_new_message':Message.has_unread_message(user.id),
                              'logged_user':user, 'act_edit_saved':is_saved,
                              'act_edit_tips':tips,
                              'action':'/activities/{0}/edit/'.format(act.id),
                              'actform' : actform,})
    return render_to_response("activity/activity_edit.html", context)

def activity_delete(request, activity_id):
    """
    Delete activity.
    """
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        return response
    
    try:
        act = Activity.objects.get(id=activity_id)
    except Activity.DoesNotExist:
#        tips = "Can't find the Activity object."
        return error_page(request, 
                          r"Error Occurs",
                          "Can't find the Activity object.")
    # Judgment has been done on template.
#    if act.creator_id != user.id and not user.is_admin:
#        #If the requester is not the creator or administrtor, do nothing.
#        return error_page(request, 
#                          r"Error Occurs",
#                          "Y")
    act.delete()
    return activity_list(request, ('success', 'You successfully delete one activity.'))

def activity_favourite(request, user_id=None):
    """
    Show list of user's favourite.
    """
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        return response
    
    if user_id is None:
        user_id = str(user.id)
        
    ###TOFIX!!!Code Below MAY Cause BUG!!!
    try: 
        suser = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return HttpResponseRedirect('/signin/')
    ###TOFIX!!!
    
    act_start = 0
    act_end = 20
    
#    acts = Activity.get_activities_by_ids(UserMeta.get_meta_value(user_id, ACTMETA_LIKE))[act_start:act_end]
    acts = Activity.get_activities_by_ids(ActivityMeta.get_activity_list(user_id, ACTMETA_LIKE))[act_start:act_end]
    
    act_list = []
    for act in acts:
        act_list.append((act,act.get_creator()))
    
    inform = SigninForm()
    context = RequestContext(request, 
                             {'STATIC_URL':STATIC_URL, 'is_myfav':user_id == str(user.id),
                              'is_act':user_id != str(user.id), 'is_logged':is_logged,
                              'has_new_message':Message.has_unread_message(user.id),
                              'logged_user':user, 'show_user':suser,
                              'activity_list':act_list, 'inform':inform,})
    return render_to_response("activity/activity_favourite.html", context)

def activity_join(request, user_id=None):
    """
    Show list of user's join.
    """
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        return response
    
    if user_id is None:
        user_id = str(user.id)
        
    ###TOFIX!!!Code Below MAY Cause BUG!!!
    try: 
        suser = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return HttpResponseRedirect('/signin/')
    ###TOFIX!!!
    
    act_start = 0
    act_end = 20
    
#    acts = Activity.get_activities_by_ids(UserMeta.get_meta_value(user_id, ACTMETA_JOIN))[act_start:act_end]
    acts = Activity.get_activities_by_ids(ActivityMeta.get_activity_list(user_id, ACTMETA_JOIN))[act_start:act_end]
    
    act_list = []
    for act in acts:
        act_list.append((act,act.get_creator()))
    
    inform = SigninForm()
    context = RequestContext(request, 
                             {'STATIC_URL':STATIC_URL, 'is_myact':user_id == str(user.id),
                              'is_act':user_id != str(user.id),
                              'is_logged':is_logged, 'logged_user':user,
                              'has_new_message':Message.has_unread_message(user.id),
                              'show_user':suser, 'activity_list':act_list,
                              'inform':inform,})
    return render_to_response("activity/activity_join.html", context)


