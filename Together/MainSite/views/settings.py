'''
Created on Apr 14, 2012  11:34:35 PM

@author:  Kangda Hu
'''
from django.http import HttpResponseRedirect
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from django.forms.forms import ErrorList

from Together.settings import STATIC_URL #@UnresolvedImport
from MainSite.models import Message #@UnresolvedImport
from MainSite.utils.form import ChgpwdForm #@UnresolvedImport
from MainSite.utils import check_user, error_page #@UnresolvedImport

def setting_detail(request):
    """
    View of setting page
    """
    return setting_password(request)

def setting_email(request):
    pass

def setting_password(request):
    """
    View for password change
    """
    (response, user, is_logged) = check_user(request, '/signin/')
    if response is not None:
        return response
    
    if request.method == 'POST':
        chpform = ChgpwdForm(request.POST)
        
        is_valid = True
        if not chpform.is_valid():
            is_valid = False
        
        try:
            if not user.check_password(chpform.cleaned_data['old_password']):
                #email not registered.
                is_valid = False
                el = ErrorList()
                el.append("Incorrect password!")
                chpform.errors['old_password'] = el
            if chpform.cleaned_data['new_password'] != chpform.cleaned_data['confirm_password']:
                #email has existed.
                is_valid = False
                el = ErrorList()
                el.append("Confirm password and password is not the same.")
                chpform.errors['confirm_password'] = el
        except AttributeError:
            pass
        
        if is_valid:
            user.set_password(chpform.cleaned_data['new_password'])
            return HttpResponseRedirect("/settings/password/password_reset_confirmation")
        
    else:
        chpform = ChgpwdForm()
    
    context = RequestContext(request, 
                    {'STATIC_URL':STATIC_URL, 'is_logged':is_logged,
                     'has_new_message':Message.has_unread_message(user.id),
                     'logged_user':user, 'is_password':True,
                     'chpform':chpform, })
    return render_to_response("setting/setting.html", context)
        
def setting_password_confirm(request):
    
#    (response, user, is_logged) = check_user(request, '/signin/')
#    if response is not None:
#        return response
#        
#    context = RequestContext(request, {'STATIC_URL': STATIC_URL,
#                                       'is_logged' : is_logged,
#                                       'logged_user' : user,})
#    return render_to_response("setting/password_reset_confirm.html", context)
    return error_page(request,
                      'Your password has been changed successfully!',
                      r'Continue <a href="/settings/">Setting</a> or back <a href="/home/">Home</a>?')

def setting_notification(request):
    """
    View for notification.
    """
    (response, user, is_logged) = check_user(request, '/signin/')
    if response is not None:
        return response
        
    context = RequestContext(request, 
                    {'STATIC_URL':STATIC_URL, 'is_logged':is_logged,
                     'has_new_message':Message.has_unread_message(user.id),
                     'logged_user':user, 'is_notify':True,})
    return render_to_response("setting/setting.html", context)

