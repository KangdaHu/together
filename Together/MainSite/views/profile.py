'''
Created on Apr 14, 2012  11:27:40 PM

@author:  Kangda Hu
'''
from django.template.context import RequestContext
from django.shortcuts import render_to_response

from Together.settings import STATIC_URL #@UnresolvedImport
from MainSite.models import ActivityMeta #@UnresolvedImport
from MainSite.models import Message #@UnresolvedImport
from MainSite.utils.form import ProfileForm #@UnresolvedImport
from MainSite.utils import check_user #@UnresolvedImport
from MainSite.settings import ACTMETA_LIKE, ACTMETA_JOIN #@UnresolvedImport

def profile_detail(request):
    """
    View for profile
    """
    
    (response, user, is_logged) = check_user(request, '/')
    if response is not None:
        return response
    
    context = RequestContext(request,
              {'STATIC_URL':STATIC_URL, 'is_home':True, 'is_logged':is_logged, 
               'is_profile':True, 'logged_user':user, 'show_user' : user,
               'has_new_message':Message.has_unread_message(user.id),
               'user_followers' : 12, 'user_following' : 10,
               'user_like' : ActivityMeta.get_activity_count(user.id, ACTMETA_LIKE),
               'user_in' : ActivityMeta.get_activity_count(user.id, ACTMETA_JOIN),
               'latest_activity_title' : "Taiwan Tour",
               'latest_activity_creator' : "Titi",
               'latest_activity_detail' : r"Just a tour to Taiwan.",
               })
    return render_to_response("user/home.html", context)

def profile_edit(request):
    """
    Edit profile
    """
    (response, user, is_logged) = check_user(request, '/')
    if response is not None:
        return response
    
    is_saved = False
    tips = ""
    
    if request.method == 'POST':
        profileform = ProfileForm(request.POST)
        
        if profileform.is_valid():
            user.nickname = profileform.cleaned_data['nickname']
            user.country = profileform.cleaned_data['country']
            user.city = profileform.cleaned_data['city']
            user.about = profileform.cleaned_data['about']
            try:
                user.save()
            except:
                tips = "Error Ocurr!"
                is_saved = False
            else:
                tips = "Save Successfully!"
                is_saved = True
            
    else:
        profile_data = {
                    'nickname' : user.nickname,
                    'country' : user.country,
                    'city' : user.city,
                    'about' : user.about}
        profileform = ProfileForm(profile_data)
        
    context = RequestContext(request, 
                {'STATIC_URL':STATIC_URL, 'is_logged':is_logged, 'logged_user':user,
                 'has_new_message':Message.has_unread_message(user.id),
                 'profile_edit_saved':is_saved, 'profile_edit_tips' : tips,
                 'profileform' : profileform, })
    return render_to_response("setting/profile_edit.html", context)



