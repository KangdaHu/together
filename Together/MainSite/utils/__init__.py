from django.http import HttpResponseRedirect
from django.template.context import RequestContext
from django.shortcuts import render_to_response

from Together.settings import STATIC_URL #@UnresolvedImport
from MainSite.auth import get_user #@UnresolvedImport

def check_user(request, direct):
    
    user = get_user(request)
    response = None
    if user is None or user.is_anonymous():
        response = HttpResponseRedirect(direct)
        is_logged = False
    else:
        is_logged = True
        
    return (response, user, is_logged)

def error_page(request, head, content):
    """
    Common template for error page.
    """
    (response, user, is_logged) = check_user(request, '/signin/')
    if response is not None:
        return response
        
    context = RequestContext(request, {'STATIC_URL': STATIC_URL,
                                       'is_logged' : is_logged,
                                       'logged_user' : user,
                                       'head' : head,
                                       'content' : content})
    return render_to_response("error_page.html", context)
